###
import scipy.optimize as opt
import math
import sys
import numpy as np
import json
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import cauchy
from sklearn.preprocessing import normalize
from scipy.stats import norm
import pandas as pd
import copy
import pickle
import random
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
###

class Noiser:
    
    ### Data description
    # The primary datastructure used in this class is a dictionary of "maps"
    # The maps are three-dimentional lists which contain noise distributions
    # relating to a pixel some distance away from the center of the event.
    # The maps are generated as follows:
    #     A set of experimentally collected images, "real images", are fit to
    #     a two-dimensional Lorentzian curve.
    #     The fractional difference between each pixel in the real image and
    #     the fit is appended to several distributions according to how far
    #     that pixel was away from the center of the fit.
    # The distributions are marshalled and unmarshalled from this map using
    # a dynamically generated intermediate datastructure which maps the pixels
    # in an images to distributions in the noise map, implemented as a pandas
    # dataframe acting as a 2-d array of tuples where the tuples contain
    # the coordinate displacement from the center of the event.
    # 
    # Since creating the dictionary of maps is very expensive,
    # the dictionary is saved as a pickle file to be loaded by default.
    # The file must be declared at instantiation of the class and should
    # be updated with the "saveDict()" method.
    
    ###
       
    def __init__(self,dictFile="dictFile.pickle",ampFile="ampFile.pickle",widthFile="widthFile.pickle",new=False,debug=False):
        self.dictFile = dictFile
        self.ampFile = ampFile
        self.widthFile = widthFile
        self.mapDict = {}
        self.ampDict = {}
        self.widthDict = {}
        self.debug = debug
        if new==False:
            self.mapDict = self.load_object(dictFile)
            self.ampDict = self.load_object(ampFile)
            self.widthDict = self.load_object(widthFile)
        
    def save_object(self,obj,fileName):
        try:
            with open(fileName, "wb") as f:
                pickle.dump(obj, f, protocol=pickle.HIGHEST_PROTOCOL)
        except Exception as ex:
            print("Error during pickling object (Possibly unsupported):", ex)
        
        
    def load_object(self,filename):
        try:
            with open(filename, "rb") as f:
                return pickle.load(f)
        except Exception as ex:
            print("Error during unpickling object (Possibly unsupported):", ex)
    
    
    ### Basic Noise Map Generation Pseudocode
    # Accept ImageSet and Energy of Images
    # Generate MapMat
    # UpdateAll MapMat from ImageSet
    # Add finished MapMat to mapDict with Energy as the key
    ###
    
    def saveDict(self):
        self.save_object(self.mapDict,self.dictFile)
        
    def saveWidth(self):
        self.save_object(self.widthDict,self.widthFile)
    
    def newMap(self, imageSet, energy):
        mapMat = self.genMapMat()
        self.updateMapAll(mapMat,imageSet)
        self.mapDict[energy] = mapMat
    
    ###
    # I'm condsidering the addition of a new map being ab-initio only
    # That is, we never update a map, we only generate a new one
    # This might help us avoid problems where values are inverted twice
    # or distributions being left unsorted etc.
    # It's an expensive operation, but should only have to be done rarely
    # and is absolutely worth avoiding data-corruption based confusion.
    ###
    
    def genMapMat(self):
        mapMat = []
        for i in range(0,33):
            mapMat.append([])
        for i in range(0,33):
            for j in range(0,33):
                mapMat[i].append([])
        return mapMat
    
    def updateMap(self, mapMat,image):
        try:
                fracDiffMat = np.array(self.diffLor(image)).reshape(16,16)
        except ValueError:
            return 0
        try:
            X, Y, Xw, Yw, amp = self.fit_please(image)
        except BadFit:
            raise BadFit
        #if X == -1 and Y == -1 and Xw == -1 and Yw == -1 and amp == -1:
        #    raise Exception
        
        # A crude way to throw away interactions too closee to edge
        # they cause bad artificial images, mostly due to bad widths
        # The Y range is smaller because of the rows of dead pixels
        # We also filter for large differences between widths in the x and y
        # if the difference is large, then it's likely a bad fit or bad image
        #if X < 1 or X > 14 or Y < 2 or Y > 13 or abs(Xw - Yw) > 2:
        #    raise Exception
        
        mapdf = self.mapNumMat(X,Y)

        for i in range(0,16):
            for j in range(0,16):
                (a,b) = mapdf[i][j][0]
                mX = 16+int(np.round(a))
                mY = 16+int(np.round(b))
                # occasionally fracDiffMat will contain a large negative number which will corrupt images if applied
                # so we make sure they are filtered here
                # There is also often a problem where the image contains very large numbers which will corrupt an image
                # in a similar way, we can filter those out here too
                if i != 0 and i != 15 and not(i == 3 and j == 13) and not(i == 7 and j == 11) and (fracDiffMat[i][j] > 0) and (fracDiffMat[i][j] < 50):
                    mapMat[mX][mY].append(
                        fracDiffMat[i][j])
    
    ## BAD Updates in place
    def updateMapAll(self, imgMap,imgSet):
        for i in range(0,len(imgSet)):
            try:
                self.updateMap(imgMap,imgSet[i])
            except BadFit:
                #print("Unable to update map from image: ",i, e)
                #break
                # This used to print a debug message, but the IO cost
                # doesn't scale well, maybe a better logging solution
                # can be done later
                continue
        imgMap = self.matSort(imgMap)
    
    def matInv(self, mat):
        invMat = self.genMapMat()
        for i in range(0,len(mat[0])):
            for j in range(0,len(mat[0])):
                invMat[i][j] = list(map(lambda x: 1/x, mat[i][j]))
        return invMat
    
    def matSort(self, mat):
        sortMat = self.genMapMat()
        for i in range(0,len(mat[0])):
            for j in range(0,len(mat[0])):
                sortMat[i][j] = sorted(mat[i][j])
        return sortMat
    
    def dist_frac(self, dist,pick):
        try:
            return dist.index(pick)/len(dist)
        except(TypeError) as e:
            print(e, pick, dist)
    
    def val_from_frac(self, dist, frac):
        index = int(np.round(frac * len(dist)))
        try:
            return dist[index]
        except(IndexError) as e:
            print(e, index, dist)
    
    
    def applyMap(self, mapMat,image):
        nimage = copy.deepcopy(image)
        X, Y, Xw, Yw, amp = self.fit_please(image)
        print(self.fit_please(image))
        print(X,Y)
        mapdf = self.mapNumMat(X,Y)

        scalingPick = self.dist_frac(mapMat[16][16],np.random.choice(mapMat[16][16]))
        for i in range(0,16):
            for j in range(0,16):
                (a,b) = mapdf[i][j]
                mX = 16+int(np.round(a))
                mY = 16+int(np.round(b))
                if i != 0 and i != 15 and not(i == 3 and j == 13) and not(i == 7 and j == 11):
                    try:
                        nimage[i][j] = nimage[i][j]*(self.val_from_frac(mapMat[mX][mY],scalingPick))
                    except(TypeError) as e:
                        print("mX, mY, i, j, X, Y\n", 
                              mX, mY, i, j, X, Y)
                        raise
                else:
                    nimage[i][j] = 0
        return nimage
    
    def getNoise(self, mapMat,image):
        nimage = copy.deepcopy(image)
        X, Y, Xw, Yw, amp = self.fit_please(image)

        mapdf = self.mapNumMat(X,Y)
        noiseMat = np.zeros((16,16),dtype=float)

        scalingPick = self.dist_frac(mapMat[16][16],np.random.choice(mapMat[16][16]))
        for i in range(0,16):
            for j in range(0,16):
                (a,b) = mapdf[i][j]
                mX = 16+int(np.round(a))
                mY = 16+int(np.round(b))
                if i != 0 and i != 15 and not(i == 3 and j == 13) and not(i == 7 and j == 11):
                    noiseMat[i][j] = (self.val_from_frac(mapMat[mX][mY],scalingPick))
        return noiseMat

    
    def diffLor(self,image, bg=False, blur=False, sigma=1):
        X, Y, Xw, Yw, amp = self.fit_please(image)

        genImage = self.genLor(amp,Y,X,Yw,Xw)
        retImg = np.asarray(image) / np.asarray(genImage)
        return retImg

    
    def twoD_Lor(self,xdata_tuple, amp,center_x, center_y, width_x, width_y):
        """Returns a lorentzian function with the given parameters"""
        (x,y) = xdata_tuple
        width_x = float(width_x)
        width_y = float(width_y)
        return ((amp*((.5*width_x)/((x-center_x)**2 + (.5*width_x)**2))) 
                * (((.5*width_y)/((y-center_y)**2 + (.5*width_y)**2)))).ravel()
    
    
    def genLor(self, amp,x,y,width_x,width_y):
        Xin, Yin = np.mgrid[0:16, 0:16]
        img = self.twoD_Lor((Xin, Yin), amp, x, y, width_x, width_y)
        return img.reshape(16,16)


    def mapNumMat(self, x,y):
        df = []
        for i in range(0,16):
            inner = []
            for j in range(0,16):
                inner.append([])
            df.append(inner)
        for i in range(0,16):
            for j in range(0,16):
                a = i - x
                b = j - y
                df[j][i].append((a,b))
        return df
    
    def paramLor(self,data):
        Xin, Yin = np.mgrid[0:16, 0:16]
        initial_guess = (10000, 8, 8, 5, 5)
        try:
            popt, pcov = opt.curve_fit(self.twoD_Lor, (Xin, Yin) , data.flatten(), p0=initial_guess)
        except:
            raise BadFit
            #return [-1,-1,-1,-1,-1]
        return popt
    
    def saveAmp(self):
        self.save_object(self.ampDict,self.ampFile)
    
    
    def newAmp(self, choices, energy):
        ampDist = self.ampDists(choices)
        self.ampDict[energy] = ampDist
    
    def ampDists(self,choices):
        choiceAmps = []
        for i in range(0,len(choices)):
            try:
                tempParams = self.fit_please(choices[i])
            except BadFit:
                continue
            #if tempParams[0] == -1 and tempParams[1] == -1 and tempParams[2] == -1 and tempParams[3] == -1 and tempParams[4] == -1:
                

        choiceAmps.append(tempParams[4])

        return np.array(choiceAmps)
    
    
    def genImage(self,maxVal,ex,ey):
        
        randi = np.random.choice(range(0,len(self.widthDict[maxVal])))
        resx = self.widthDict[maxVal][0][randi]
        resy = self.widthDict[maxVal][1][randi]
        
        Xin, Yin = np.mgrid[0:16, 0:16]
        mu, std = norm.fit(self.ampDict[maxVal])
        if self.debug:
            print("maxVal: ", maxVal, "mu: ", mu)
        img = self.twoD_Lor((Xin, Yin), mu, ex, ey, resx, resy)
        return img.reshape(16,16)
    
    
    
    
    
    
    def genRandImage(self):
        ##DEBUG
        double = 0
        
        image = np.zeros((16,16),dtype=float)
        double = np.random.randint(1,3)
        #if double == 1
        for i in range(0,double):
            eRange = list(self.mapDict.keys())
            yRange = range(1,16)
            if i == 0:
                energy1 = random.choice(eRange)
            else:
                energy2 = random.choice(eRange)
            if i == 0:
                y1 = random.choice(yRange)
                x1 = random.choice(range(0,16))
            else:
                y2 = random.choice(yRange)
                x2 = random.choice(range(0,16))
            try:
                if i == 0:
                    image += self.applyMap(self.mapDict[energy1],self.genImage(energy1,x1,y1))
                else:
                    image += self.applyMap(self.mapDict[energy2],self.genImage(energy2,x2,y2))
            except:
                return self.genRandImage()
        if double == 2:
            return [image,double-1,[[x1,y1],[x2,y2]],[energy1,energy2]]
        if double == 1:
            return [image,double-1,[x1,y1],energy1]
        else:
            print("WHAT",i,double)
            return [image,double-1,[x1,y1]]
        
    def ifilter(self, x,y,xw,yw,amp):
        return amp > 1 and abs(xw - yw) < 2 and x > 1 and x < 15 and y > 2 and y < 14 and xw > 1 and yw > 1
        
    def fit_please(self, data):
        Xin, Yin = np.mgrid[0:16, 0:16]
        initial_guess = (10000, 8, 8, 5, 5)
        # I suspect it isn't bounds I want, I need to limit what the program attempts to fit
        #bounds = ( 0, [np.inf, 15, 15, np.inf, np.inf])
        try:
            popt, pcov = opt.curve_fit(self.twoD_Lor, (Xin, Yin) , data.flatten(), 
                                      p0=initial_guess)
                                      #method='dogbox',
                                      #ftol=None,
                                      #gtol=None)
        except RuntimeError:
            raise BadFit
            #return [-1, -1, -1, -1, -1]

        (height, Y, X, width_y, width_x) = popt
        
        if not self.ifilter(X,Y,width_x,width_y,height):
            raise BadFit
            #return [-1, -1, -1, -1, -1]
        
        #fl = noiser.twoD_Lor((Xin,Yin), height, Y, X, width_y, width_x)
        #fls = fl.reshape(16,16)
        #fls[0] = np.repeat(0.,16)
        #fls[15] = np.repeat(0.,16)
        #r = data.flatten() - fls.flatten()
        #chisq = np.sum((r/np.repeat(100.,len(data.flatten())))**2)
        
        #if chisq > 500:
        #    return [-1, -1, -1, -1, -1]
        
        #if not ifilter(X,Y,width_x,width_y,height):
        #    return [-1, -1, -1, -1, -1]
    
        return (X, Y, width_x, width_y, height)
    
    def genAvgMap(self, key):
        mapMat = self.mapDict[key]
        avgMap = np.zeros((33,33),dtype=float)
        for i in range(0,33):
            for j in range(0,33):
                avgMap[i][j] = np.average(mapMat[i][j])
        return avgMap
        
        
    def genDensityMap(self, key):
        mapMat = self.mapDict[key]
        avgMap = np.zeros((33,33),dtype=float)
        for i in range(0,33):
            for j in range(0,33):
                avgMap[i][j] = len(mapMat[i][j])
        return avgMap
        
    def widthDists(self, choices, low = 0, high = 10):
        choiceParams = []
        for i in range(0,len(choices)):
            try:
                tempParams = self.fit_please(choices[i])
            except BadFit:
                continue
            #if np.any(tempParams == [-1,-1,-1,-1,-1]):
            #    continue
            choiceParams.append(tempParams)
        choiceParamsx = [abs(x[2]) for x in choiceParams if x[2] > low and x[2] < high]    
        choiceParamsy = [abs(x[3]) for x in choiceParams if x[3] > low and x[3] < high]

        return np.array(choiceParamsx), np.array(choiceParamsy)
    
    def ampDists(self, choices):
        choiceParams = []
        for i in range(0,len(choices)):
            try:
                tempParams = self.fit_please(choices[i])
            except BadFit:
                continue
            #if np.any(tempParams == [-1,-1,-1,-1,-1]):           
            #    continue
            choiceParams.append(tempParams)
        choiceAmps = [x[4] for x in choiceParams]    

        return np.array(choiceAmps)
    
    
    def chosenTot(self, imageset, sumPixel=3500, tol=.1):
        choice = []
        for image in imageset:
            if np.sum(image) <= sumPixel + sumPixel*tol and np.sum(image) >= sumPixel - sumPixel*tol:
                (x,y) = np.where(image == np.amax(image))
                x = x[0]
                y = y[0]
                if x >= 3 and x <= 12 and y >= 3 and y <= 12:
                    choice.append(image)
        return choice


    def chosenCenter2(self, imageset):
        choice = []
        for image in imageset:
            (x,y) = np.where(image == np.amax(image))
            x = x[0]
            y = y[0]
            if x >= 6 and x <= 9 and y >= 6 and y <= 9:
                choice.append(image)
        return choice
    
class BadFit(Exception):
    pass
